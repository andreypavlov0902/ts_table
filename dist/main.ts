import * as $ from 'jquery';
import Tether from 'tether';
import "bootstrap";

import * as Figures from './figure';
import * as Form from './ajax';
import {SaveData} from './localStorage';


let history :any = new SaveData();
let arrFig: any[] = [];

interface figureName{
    name: string;
    value: number;
}
export enum figureList{
    circle,
    triangle,
    square,
    rectangle
}
//console.log('figureList.circle', figureList.circle) // number
//console.log('figureList.circle', figureList[figureList.circle])// string

// ajax form
let form: any = new Form.GetNeedForm(
    figureList.circle,
    figureList.triangle,
    figureList.square,
    figureList.rectangle
);

let figure: figureName[] = [
    {
        name: figureList[figureList.circle],
        value: figureList.circle
    },
    {
        name: figureList[figureList.triangle],
        value: figureList.triangle
    },
    {
        name: figureList[figureList.square],
        value: figureList.square
    }, {
        name: figureList[figureList.rectangle],
        value: figureList.rectangle
    }
];

class Views{

    protected figureElement: number;
    private panelOne: any = $('.select-figure');
    private panelTwo: any = $('.figure-params');

    constructor(protected defaultFigure: number){
        this.figureElement = this.defaultFigure;
        this.getHistory();
    }
// repeat options in select
   public repeat(selector: string, lists: figureName[]):any{
        let element: any = $(selector);

        for(let value of lists){
            let elementOption: any;

            if(this.defaultFigure == value.value){
                elementOption = $(`<option value="${value.value}" selected >${value.name}</option>`);
            }else{
                elementOption = $(`<option value="${value.value}">${value.name}</option>`);
            }

            element.append(elementOption);
        }
    }
//jobs with history
    public getHistory(){
        let arr: any = history.getData('ArrayFigure');
        if(arr){
            arrFig.push(...arr);
            this.repeatTable()
        }
    }
    // append html in DOM
    public addFormInHtml(arr: any, selector: string):any{
        $(selector).empty();
        for(let obj of arr){
            let element: any = `<div class="form-group">
                            <label for="${obj.for}">${obj.placeholder}</label>
                            <input type="${obj.type}" ${obj.disabled} required class="form-control" id="${obj.id}" name="${obj.name}" aria-describedby="Type" placeholder="${obj.placeholder}">
                            </div>`;
            $(selector).append(element);
        }
    }

    public hideAny(selector: string): any{
        $(selector).hide();
    }

    public showAny(selector: string): any{
        $(selector).show();
    }
    // text property choice figure
    public getFigureName(num: number){
        return figureList[num];
    }
    // add value in form
    public addSeltctType(typeFigure: number):any{
        $('#data-figure').find('input[id=type]').val(figureList[typeFigure])
    }

    public closeModal(selector: string){
        $(selector).modal('hide');
    }

    public deleteTR(){
        let self: any = this;
        $('.delete').on('click', function(){
            let id: string = $(this).prop('id');
            self.delObjInArr(id);
            let tr: any =  $(this).closest('tr').remove();
            self.repeatTable();
        })
    }

    protected delObjInArr(id: number){
        arrFig.splice(id, 1);
        history.setDataArray("ArrayFigure", arrFig);
    }
    protected ArrToObj(arr, type: string){
        let obj: any ={type: type};
        arr.forEach((index)=>{
            obj[index.name] = index.value;
        });

        return obj;
    }
    protected addSquare(arr: any, type: string){

        let square: any = new Figures.Square(this.ArrToObj(arr, type));
        arrFig.push(square.getAllProperti());
        history.setDataArray("ArrayFigure", arrFig);
    }
    protected addRectangle(arr: any, type: string){
        let rectangle: any = new Figures.rectangle(this.ArrToObj(arr, type));
        arrFig.push(rectangle.getAllProperti());
        history.setDataArray("ArrayFigure", arrFig);
    }
    protected addTriangle(arr: any, type: string){
        let triangle: any = new Figures.triangle(this.ArrToObj(arr, type));
        arrFig.push(triangle.getAllProperti());
        history.setDataArray("ArrayFigure", arrFig);
    }
    protected addCircle(arr: any, type: string){
        let circle: any = new Figures.Circle(this.ArrToObj(arr, type));
        arrFig.push(circle.getAllProperti());
        history.setDataArray("ArrayFigure", arrFig);

    }
    protected writePropertyInFigure(arr: any){
        let figureName: string = this.getFigureName(this.figureElement);
        switch (figureName){
            case 'square':
                this.addSquare(arr, figureName);
                break;
            case 'rectangle':
                this.addRectangle(arr, figureName);
                break;
            case 'triangle':
                this.addTriangle(arr, figureName);
                break;
            case 'circle':
                this.addCircle(arr, figureName);
                break;
        }
    }


    protected addHandler(selector: string, event: string){

        $(selector).on(event, $.proxy( (e)=> {
            e.preventDefault();
            let result = $('#data-figure').serializeArray();
            this.writePropertyInFigure(result);
            this.closeModal('#exampleModalLong')
            this.repeatTable();
        },this))
    }

    public triggerForm(selector: string){
       let self:any = this;
       let elem: any = $(selector);

       elem.on('click',  ()=> {
           this.panelOne.hide();
           this.panelTwo.show();

           $('#exampleModalLong').on('hidden.bs.modal', ()=> {
               this.closePopUp();
           });


         form.choiceForm(this.figureElement).then((data)=>{
             this.addFormInHtml(data, '#data-figure');

             this.addSeltctType(this.figureElement);

             this.hideAny('#save');

             $('#data-figure').find('input').each(function () {
                 $(this).on('blur',$.proxy(self.showButtonAdd,self) );
             });

             this.addHandler('#save', 'click');
           });
       })

    }

    public closePopUp():any{

        $('#data-figure').find('input').each(function () {
            $(this).val(null);
        });
        this.panelOne.show();
        this.panelTwo.hide();
    }

    public changeSelect(): any{
        let section: any = $('.figure');

        section.on('change', () => {
           this.figureElement = section.val();
        })
    }

    public showButtonAdd(){
        let self: any = this;
        let arrValue: any[] =[];
        let result: boolean;

        $('#data-figure').find('input').each(function () {
            arrValue.push($(this).val());
        });
        result = arrValue.every((item)=>{
            return item !== '';
        });

        if(result){
            self.showAny('#save');
        }else {
           self.hideAny('#save');
        }
    }
    createTR(item, i){
        let tr: any = $('<tr>');
        tr.html(`<td>${item.name}</td>
                     <td>${item.type}</td>
                      <td>${item.cord_x}</td>
                      <td>${item.cord_y}</td>
                      <td>${item.perimeter}</td>
                      <td>${item.area}</td>
                      <td>${item.width}</td>
                      <td>${item.height}</td>
                      <td>${item.radius}</td>
                      <td><button class="btn bg-danger delete" id="${i}">DELETE</button></td>
                      `);
        return tr;
    }

    public repeatTable(): any{

            let tbody: any = $('<tbody>');
        $('#table').find('tbody tr').remove();
        arrFig.forEach((item, i)=>{
            tbody.append(this.createTR(item, i))
        })
            $('#table').append(tbody);
        this.deleteTR();
    }
}

let views:any = new Views(figureList.square);

views.repeat('.figure', figure);
views.triggerForm('.Enter-figure');
views.changeSelect();


