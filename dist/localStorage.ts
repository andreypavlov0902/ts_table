/**
 * Created by andrei on 14.05.2017.
 */

export class SaveData{
    constructor(){}

    getDataFromLocalStorage(key: string):any{
      return  localStorage.getItem(key);
    }

    getData(key: string){
        return JSON.parse(this.getDataFromLocalStorage(key))
    }

    setDataArray(key: string, data: any){
        if(data){
         return localStorage.setItem( key, this.dataStringify(data))
        }
    }
    dataStringify(data: any){
        return  JSON.stringify(data)
    }


}
