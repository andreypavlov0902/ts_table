export abstract class figure {
    name: string;
    type: string;
    x: number;
    y: number;
    height: number|string;
    width: number|string ;
    radius: number|string;

    constructor({name, type, x, y, height = '-', width = '-', radius = '-'}){

        this.name = name;
        this.type = type;
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.radius = radius;
    }
    perimeter(){

    }
    area(){

    }

    getAllProperti(): any{
        return{
            name: this.name,
            type: this.type,
            cord_x: this.x,
            cord_y: this.y,
            height: this.height,
            width: this.height,
            radius: this.radius,
            perimeter: this.perimeter(),
            area: this.area()
        }
    }
}

export class Square extends figure{
    height: number;
    constructor({name, type, x, y, height , width = '-', radius = '-'}){
        super({name, type, x, y, height, width, radius})
        this.height = height;
        this.width = height;
    }

    perimeter(){
            return 4 * this.height;
    }
    area(){
       return this.height * this.height;
    }


}


export class Circle extends figure{

    radius: number;
    constructor({name, type, x, y, height = '-' , width = '-', radius }){
        super({name, type, x, y, height, width, radius})
        this.radius = radius;
    }

     perimeter():any{
        return 2 * 3.14 * this.radius;
    }
     area():any{
        return 3.14 * Math.pow(this.radius, 2);
    }

}

export class triangle extends figure{
    height: number;
    constructor({name, type, x, y, height , width = '-', radius = '-'}){
        super({name, type, x, y, height, width, radius})
        this.height = height;
    }

     perimeter():any{
        return (Math.sqrt(3)/4) * Math.pow(this.height,2);
    }
     area():any{
        return 3 * this.height;
    }

}
export class rectangle extends figure{
    width: number;
    height: number;
    constructor({name, type, x, y, height , width, radius = '-'}){
        super({name, type, x, y, height, width, radius})
        this.height = height;
    }
    perimeter():number{
        return (2 * this.height) + (2 * this.width);
    }
    area():number{
        return this.height * this.width;
    }

}

