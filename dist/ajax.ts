/**
 * Created by andrei on 13.05.2017.
 */
import {figureList} from './main';

export class GetNeedForm{
    htmlForm: any;
    formName: string;
    circle: figureList;
    triangle: figureList;
    square: figureList;
    rectangle: figureList;

    constructor(  circle: figureList, triangle: figureList, square: figureList, rectangle: figureList){
        this.circle = circle;
        this.triangle = triangle;
        this.square = square;
        this.rectangle = rectangle;
    }

    choiceForm(id: number){
        this.formName = figureList[id];
        return this.getHtml(this.formName)

    }
    getFormName():string{
        return this.formName;
    }
    requestForm(name: string): any{

        let self: any = this;
       return $.ajax({
                url: `./json/${name}.json`,
                type: 'GET',
                dataType: 'json',
            success:function (data):any{
               return self.htmlForm = data;
            }
        });
    }

    getHtml(name: string):any{
      return new Promise((resolve:any, reject:any):any=>{
            resolve(this.requestForm(name));
        });
    }
}

