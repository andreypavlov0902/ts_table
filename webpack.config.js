var webpack = require('webpack');
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    context: path.resolve(__dirname, './'), // New line
    devServer: {
        contentBase: path.resolve(__dirname, 'app'), // New line
    },
    entry: {
        index: './dist/main.ts',

    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'app'),
        publicPath: './',
        sourceMapFilename: '[file].map',
      
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: [ ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },
    module: {
        loaders: [
            // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
            { test: /\.tsx?$/, loader: "ts-loader" }
        ],
        /*rules: [

            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        { loader: 'css-loader', options: { modules: false, url: false } },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [require('autoprefixer')];
                                },
                            },
                        },
                        'sass-loader'
                    ]
                }),
            },
        ]*/
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "Tether": 'tether'
        }),
        new CopyWebpackPlugin([
            {from: 'json/**/*'},
        ]),
        new CopyWebpackPlugin([
            {from: 'css/**/*'},
        ]),
        new HtmlWebpackPlugin({
            title: 'Index-html',
            filename: 'index.html',
            template: 'index.html',
            chunks: ['index']
        }),
    ]
}
/*   пример что можно еще использовать
new webpack.ProvidePlugin({
    '$':          'jquery',
    '_':          'lodash',
    'ReactDOM':   'react-dom',
    'cssModule':  'react-css-modules',
    'Promise':    'bluebird'
})*/
